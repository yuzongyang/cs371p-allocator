// -----------
// Allocator.h
// -----------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument
#include <iostream>
using namespace std;
// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;}                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);}

    public:
        // --------
        // typedefs
        // --------

        using      value_type = T;

        using       size_type = std::size_t;
        using difference_type = std::ptrdiff_t;

        using       pointer   =       value_type*;
        using const_pointer   = const value_type*;

        using       reference =       value_type&;
        using const_reference = const value_type&;

    public:
        // ---------------
        // iterator
        // over the blocks
        // ---------------

        class iterator {
            // -----------
            // operator ==
            // -----------

            friend bool operator == (const iterator& lhs, const iterator& rhs) {
                // <your code>
                return (lhs._p == rhs._p);}                                           // replace!

            // -----------
            // operator !=
            // -----------

            friend bool operator != (const iterator& lhs, const iterator& rhs) {
                return !(lhs._p == rhs._p);}

            private:
                // ----
                // data
                // ----

                int* _p;

            public:
                // -----------
                // constructor
                // -----------

                iterator (int* p) {
                    _p = p;}

                // ----------
                // operator *
                // ----------

                int& operator * () const {
                    return *_p;} 
                // -----------
                // operator ++
                // -----------

                iterator& operator ++ () {
                    int incr = abs(*_p);
                    char* &char_p = reinterpret_cast<char*&>(_p);
                    char_p += (incr + 8);
                    return *this;}

                // -----------
                // operator ++
                // -----------

                iterator operator ++ (int) {
                    iterator x = *this;
                    ++*this;
                    return x;}

                // -----------
                // operator --
                // -----------

                iterator& operator -- () {
                    char* &char_p = reinterpret_cast<char*&>(_p);
                    char_p -= 4;
                    int incr = abs(*_p);
                    char_p -= (incr + 4);
                    return *this;}

                // -----------
                // operator --
                // -----------

                iterator operator -- (int) {
                    iterator x = *this;
                    --*this;
                    return x;}};

        // ---------------
        // const_iterator
        // over the blocks
        // ---------------

        class const_iterator {
            // -----------
            // operator ==
            // -----------

            friend bool operator == (const const_iterator& lhs , const const_iterator& rhs) {
                // <your code>
                return (lhs._p == rhs._p);}                                                       // replace!

            // -----------
            // operator !=
            // -----------

            friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
                return !(lhs._p == rhs._p);}

            private:
                // ----
                // data
                // ----

                const int* _p;

            public:
                // -----------
                // constructor
                // -----------

                const_iterator (const int* p) {
                    _p = p;}

                // ----------
                // operator *
                // ----------

                const int& operator * () const {
                    // <your code>
                    const int& tmp = *_p;
                    return tmp;}                

                // -----------
                // operator ++
                // -----------

                const_iterator& operator ++ () {
                    int incr = abs(*_p);
                    const char* &char_p = reinterpret_cast<const char*&>(_p);
                    char_p += (incr + 8);
                    return *this;}
                // -----------
                // operator ++
                // -----------

                const_iterator operator ++ (int) {
                    const_iterator x = *this;
                    ++*this;
                    return x;}

                // -----------
                // operator --
                // -----------

                const_iterator& operator -- () {
                    const char* &char_p = reinterpret_cast<const char*&>(_p);
                    char_p -= 4;
                    int incr = abs(*_p);
                    char_p -= (incr + 4);
                    return *this;}

                // -----------
                // operator --
                // -----------

                const_iterator operator -- (int) {
                    const_iterator x = *this;
                    --*this;
                    return x;}
                };

    private:
        // ----
        // data
        // ----

        char a[N];

        // -----
        // valid
        // -----

        /**
         * O(1) in space
         * O(n) in time
         * check if its filled with valid blocks and no two
         * consecutive blocsk are free
         */
        bool valid () const {
            // <use iterators>
            int total = 0;
            bool free = false;
            const_iterator b = begin();
            const_iterator e = end();
            // loop to check if each block is valid 
            while (b != e) {
                if (*b > 0) {
                    if (free) {
                        return false;
                    }
                    total += (*b + 8);
                    free = true;
                }
                else {
                    total -= *b - 8;
                    free = false;
                }
                ++b;
            }
            if (b == e) {
                total += abs(*b) + 8;
            }
            if (total != N) {
                return false;
            }
            return true;
        }

    public:
        // -----------
        // constructor
        // -----------

        /**
         * O(1) in space
         * O(1) in time
         * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
         */
        my_allocator () {
            if (N < sizeof(T) + (2 * sizeof(int))) {
                throw std::bad_alloc();
            }
            reinterpret_cast<int*>(a)[0] = N - 8;
            reinterpret_cast<int*>(a)[N / 4 - 1] = N - 8;
            // <your code>
            assert(valid());
        }

        my_allocator             (const my_allocator&) = default;
        ~my_allocator            ()                    = default;
        my_allocator& operator = (const my_allocator&) = default;

        // --------
        // allocate
        // --------

        /**
         * O(1) in space
         * O(n) in time
         * after allocation there must be enough space left for a valid block
         * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
         * choose the first block that fits
         * throw a bad_alloc exception, if n is invalid
         */
        pointer allocate (size_type s) {
            // <your code>
            int numByte = s * sizeof(T);
            if (numByte == 0) {
                return nullptr;
            }
            if (numByte + 2 * sizeof(int) < sizeof(T)) {
                throw std::bad_alloc();
            }
            if (numByte > 0) {
                iterator b = begin();
                iterator e = end();
                bool filled = false;
                bool fin = false;
                do {
                    if (b == e) {
                        fin = true;
                    }
                    if (*b >= numByte) {
                        int extra = *b - numByte;
                        if (extra >= sizeof(T) + (2 * sizeof(int))) {
                            extra -= 8;
                            *b = -numByte;
                            int* endSentinal1 = &(*b) + (numByte / 4) + 1;
                            *(endSentinal1) = -numByte;
                            ++b;
                            *b = extra;
                            int* endSentinal2 = &(*b) + (extra / 4) + 1;
                            *(endSentinal2) = extra;
                            filled = true;
                        }
                        else {
                            int temp = *b;
                            *b = -temp;
                            int* endSentinal = &(*b) + (temp / 4) + 1;
                            *(endSentinal) = -temp;
                            filled = true;
                        }
                    }
                    else {
                        ++b;
                    }
                } while (!filled && !fin);
            }
            assert(valid());
            return (pointer)(&(*begin()) + 1);
        }             // replace!

        // ---------
        // construct
        // ---------

        /**
         * O(1) in space
         * O(1) in time
         */
        void construct (pointer p, const_reference v) {
            new (p) T(v);                               // this is correct and exempt
            assert(valid());
            }                           // from the prohibition of new

        // ----------
        // deallocate
        // ----------

        /**
         * O(1) in space
         * O(1) in time
         * after deallocation adjacent free blocks must be coalesced
         * throw an invalid_argument exception, if p is invalid
         */
        void deallocate (pointer p , size_type s) {
            iterator b = iterator((int*)(p) - 1);
            // check if in bounds
            if (*b < s) {
                throw std::invalid_argument("break");
            }
            *b *= -1;
            bool left = false;
            bool right = false;
            // free the block and coalesce
            if (b != begin()) {
                left = (*--b > 0);
                ++b;
            }
            if (b != end()) {
                right = (*++b > 0);
                --b;
            }
            if (left || right) {
                coalesced(b, left, right);
            }
            assert(valid());
        }


        /**
         * O(1) in space
         * O(1) in time
         * combine after being deallocated
         */
        void coalesced (iterator iter, bool l, bool r) {
            // check the left if positive
            if (l) {
                int numBytes = *iter + 8;
                --iter;
                numBytes += *iter;
                *iter = numBytes;
                ++iter;
                int* footer = &(*iter) - 1;
                *(footer) = numBytes;
            }
            if(l && r) {
                --iter;
            }
            // check the right side if positive
            if (r) {
                int numBytes = *iter + 8;
                ++iter;
                numBytes += *iter;
                --iter;
                *iter = numBytes;
                ++iter;
                int* footer = &(*iter)-1;
                *(footer) = numBytes;
            }
        }

        
        void deallocateH (size_type s) {
            s *= -1;
            iterator b = begin();
            iterator block = begin();
            iterator e = end();
            size_type num = 0;
            bool fin = false;
            // loop and check num of blocks that are negative
            while (!fin && num < s) {
                if (b == e) {
                    fin = true;
                }
                if (*b <= 0) {
                    block = b;
                    ++num;
                }
                ++b;
            }
            deallocate((pointer)(&(*block) + 1), *block);
        }
        // -------
        // destroy
        // -------

        /**
         * O(1) in space
         * O(1) in time
         */
        void destroy (pointer p) {
            p->~T();               // this is correct
            assert(valid());
        }

        // -----------
        // operator []
        // -----------

        /**
         * O(1) in space
         * O(1) in time
         */
        int& operator [] (int i) {
            return *reinterpret_cast<int*>(&a[i]);
        }

        /**
         * O(1) in space
         * O(1) in time
         */
        const int& operator [] (int i) const {
            return *reinterpret_cast<const int*>(&a[i]);
        }

        // -----
        // begin
        // -----

        /**
         * O(1) in space
         * O(1) in time
         */
        iterator begin () {
            return iterator(&(*this)[0]);
        }

        /**
         * O(1) in space
         * O(1) in time
         */
        const_iterator begin () const {
            return const_iterator(&(*this)[0]);
        }

        // ---
        // end
        // ---

        /**
         * O(1) in space
         * O(1) in time
         */
        iterator end () {
            return iterator(&(*this)[N-8-abs((*this)[N-4])]);
        }

        /**
         * O(1) in space
         * O(1) in time
         */
        const_iterator end () const {
            return const_iterator(&(*this)[N-8-abs((*this)[N-4])]);
        }
};

#endif // Allocator_h
