// ----------------
// RunAllocator.c++
// ----------------

// --------
// includes
// --------

#include <iostream> // cin, cout

#include "Allocator.hpp"

using namespace std;

// --------------
// allocator_read
// --------------

istream& allocator_read (istream& read, my_allocator<double, 1000>& a, bool eof) {
	int required;
	string str; 
	if (!eof) {
		getline(read, str);
		while(str.compare("")) {
			required = stoi(str);
			if (required >= 0) {
				a.allocate(required);
			}
			else {
				a.deallocateH(required);
			}
			getline(read, str);
		}
	}
	else {
		getline(read, str);
		bool fin = false;
		while(!fin) {
			required = stoi(str);
			if (required >= 0) {
				a.allocate(required);
			}
			else {
				a.deallocateH(required);
			}
			getline(read, str);
			if (cin.eof()) {
				fin = true;
			}
		}
	}
	return read;
}



// ---------------
// allocator_print
// ---------------

void allocator_print (my_allocator<double, 1000>& a) {
	my_allocator<double, 1000>::iterator b = a.begin();
	my_allocator<double, 1000>::iterator e = a.end();
	while (b != e) {
		cout << *b << " ";
		++b;
	}
	cout << *b << endl;
}


// ---------------
// allocator_solve
// ---------------

void allocator_solve (istream& sin, ostream& sout) {
	int n;
	sin >> n;
	string temp; 
	getline(sin, temp);
	getline(sin, temp);
	for (int i = 0; i < n; ++i) {
		my_allocator<double, 1000> a;
		if (i < n - 1) {
			allocator_read(sin, a, false);
		}
		else {
			allocator_read(sin, a, true);
		}
		allocator_print(a);
	}
}

// ----
// main
// ----

int main () {
    
    /*
    your code for the read eval print loop (REPL) goes here
    in this project, the unit tests will only be testing Allocator.hpp, not the REPL
    the acceptance tests will be testing the REPL
    */
    allocator_solve(cin, cout);
    return 0;}
